package zipex

import (
	"archive/zip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestInPlace(t *testing.T) {
	// The zip file name
	tzfn := filepath.Join(os.TempDir(), fmt.Sprintf("%d-inplace-test.zip", time.Now().UnixNano()))

	{
		// Create a file
		f, err := os.Create(tzfn)

		// Check for an error
		if err != nil {
			t.Fatal("Unexpected error:", err)
		} //if

		// Close the file
		defer f.Close()

		// Create the writer
		w := zip.NewWriter(f)

		// Write a file to the zip writer
		zf, err := w.Create("a.txt")

		// Check for an error
		if err != nil {
			t.Fatal("Unexpected error:", err)
		} //if

		// Write contents
		if _, err = io.WriteString(zf, "A"); err != nil {
			t.Fatal("Unexpected error:", err)
		} //if

		// Close the writer
		w.Close()
	}

	err := InPlace(tzfn, func(w *zip.Writer, r *zip.Reader) error {
		// Create a new file
		zf, err := w.Create("b.txt")

		// Check for an error
		if err != nil {
			t.Fatal("Unexpected error:", err)
		} //if

		if _, err = io.WriteString(zf, "B"); err != nil {
			t.Fatal("Unexpected error:", err)
		} //if

		return nil
	})

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Open the file
	rc, err := zip.OpenReader(tzfn)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check for the files
	if expected, actual := 1, len(rc.Reader.File); actual != expected {
		t.Fatalf("Expected %d files but was %d", expected, actual)
	} //if

	// Chehck the name of the first one
	if expected, actual := "b.txt", rc.Reader.File[0].Name; actual != expected {
		t.Fatalf(`Expected name "%s" but was "%s"`, expected, actual)
	} //if

	// Close the reader
	if err = rc.Close(); err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Delete the temporary file
	os.Remove(tzfn)
} //func
