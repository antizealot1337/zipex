package zipex

import (
	"archive/zip"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestCopyEach(t *testing.T) {
	// Open the test zip file
	rc, err := zip.OpenReader(filepath.Join("testdata", "abc.zip"))

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	defer rc.Close()

	// The temp file name
	tfn := filepath.Join(os.TempDir(), time.Now().String()+"0go-zip-test.zip")

	// Create the file
	f, err := os.Create(tfn)

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	defer func() {
		if err = os.Remove(tfn); err != nil {
			t.Fatal("Unexpected error:", err)
		} //if
	}() //func

	// Create a file for writing
	zw := zip.NewWriter(f)

	// Copy all but a.txt
	err = CopyEach(zw, &rc.Reader, func(f *zip.File) bool {
		return f.Name != "a.txt"
	}) //func

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Close the writer
	if err = zw.Close(); err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Open the temp file for reading
	rc, err = zip.OpenReader(tfn)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	var names []string

	// Get the file names
	for _, f := range rc.Reader.File {
		names = append(names, f.Name)
	} //for

	// Check the number of files
	if expected, actual := 2, len(names); actual != expected {
		// Close the reader and don't care about any error as the file
		// will be deleted
		rc.Close()

		t.Fatalf("Expected %d files but there were %d", expected, actual)
	} //if

	// Check the first file name
	if expected, actual := "b.txt", names[0]; actual != expected {
		t.Errorf(`Expected name "%s" but was "%s"`, expected, actual)
	} //if

	// Check the second file name
	if expected, actual := "c.txt", names[1]; actual != expected {
		t.Errorf(`Expected name "%s" but was "%s"`, expected, actual)
	} //if
} //func
