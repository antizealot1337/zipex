package zipex

import (
	"archive/zip"
	"errors"
	"io"
)

// CopyEach files in the zip.Reader to the zip.Writer if include function returns true.
func CopyEach(dst *zip.Writer, orig *zip.Reader, include func(*zip.File) bool) error {
	// Loop through the files
	for _, f := range orig.File {
		// Check if the files should be copied
		if !include(f) {
			continue
		} //if

		// Open the file
		rc, err := f.Open()

		// Check for an error
		if err != nil {
			return errors.New("copy open error: " + err.Error())
		} //if

		// Close the reader later
		defer rc.Close()

		// Create a writer for the destination
		w, err := dst.CreateHeader(&f.FileHeader)

		// Check for an error
		if err != nil {
			return errors.New("copy create file error: " + err.Error())
		} //if

		// Copy the contents
		_, err = io.Copy(w, rc)

		// Check for an error
		if err != nil {
			return errors.New("copy data error: " + err.Error())
		} //if
	} //for

	return nil
} //func
