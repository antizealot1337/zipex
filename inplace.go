package zipex

import (
	"archive/zip"
	"errors"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

// InPlace will modify a zip file "in place". It actually creates a temporary
// zip files, performs nodifications, and replace the original.
func InPlace(zipfile string, modify func(*zip.Writer, *zip.Reader) error) error {
	// Open a reader
	zipr, zipc, err := zipreader(zipfile)

	// Check for an error
	if err != nil {
		return errors.New("inplace error: reader: " + err.Error())
	} //if

	// Close at exit
	defer zipc.Close()

	tempfile := filepath.Join(os.TempDir(), strconv.FormatInt(time.Now().UnixNano(), 16)+"temp.zip")

	// Open a writer
	zipw, tmpc, err := zipwriter(tempfile)

	// Check for an error
	if err != nil {
		return errors.New("inplace error: writer: " + err.Error())
	} //if

	// Close at exit
	defer tmpc.Close()

	// Call modify
	if err = modify(zipw, zipr); err != nil {
		return errors.New("inplace error: modify: " + err.Error())
	} //if

	// Close the reader and writer
	if err = zipc.Close(); err != nil {
		return errors.New("inplace error: orig zip close: " + err.Error())
	} //if

	if err = tmpc.Close(); err != nil {
		return errors.New("inplace error: temp zip close: " + err.Error())
	} //if

	// Delete the original
	if err = os.Remove(zipfile); err != nil {
		return errors.New("inplace error: remove origin: " + err.Error())
	} //if

	// Move the temporary one
	if err = os.Rename(tempfile, zipfile); err != nil {
		return errors.New("inplace error: move: " + err.Error())
	} //if

	return nil
} //func

func zipreader(file string) (*zip.Reader, io.Closer, error) {
	// Open the zip reader
	rc, err := zip.OpenReader(file)

	// Check for an error
	if err != nil {
		return nil, nil, errors.New("zip reader open error: " + err.Error())
	} //if

	// Create the closer
	closer := newOnceCloser(rc)

	return &rc.Reader, closer, nil
} //func

func zipwriter(file string) (*zip.Writer, io.Closer, error) {
	// Open the file
	f, err := os.Create(file)

	// Check for an error
	if err != nil {
		return nil, nil, errors.New("zip writer open file error: " + err.Error())
	} //if

	// Create a writer
	w := zip.NewWriter(f)

	// Create a closer
	closer := newOnceCloser(closeFn(func() error {
		var err error
		if err = w.Close(); err != nil {
			return err
		} //if
		if err = f.Close(); err != nil {
			return err
		} //if
		return nil
	}))

	return w, closer, nil
} //func

type closeFn func() error

func (c closeFn) Close() error { return c() } //func

type onceCloser struct {
	c io.Closer
	o sync.Once
} //struct

func newOnceCloser(c io.Closer) *onceCloser {
	return &onceCloser{c: c}
} //func

// Close the Closer.
func (o onceCloser) Close() (err error) {
	o.o.Do(func() { err = o.c.Close() })
	return
} //func
